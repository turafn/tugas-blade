@extends('master')

@section('content')
<div class="mt-3 mb-3 mr-3 ml-3">
    <div class="alert alert-primary" role="alert">
  Halaman Detail
</div>
<form action="/pertanyaan">
  <div class="form-group">
    <label for="judul">Judul</label>
    <input type="text" class="form-control" id="judul"  value="{{ $pertanyaan->judul }}" readonly>
  </div>
  <div class="form-group">
    <label for="isi">Isi</label>
    <input type="text" class="form-control" id="isi" value="{{ $pertanyaan->isi }}" readonly>
  </div>
  <button type="submit" class="btn btn-primary">Kembali</button>
</form>
</div>
@endsection