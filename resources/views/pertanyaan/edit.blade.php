@extends('master')

@section('content')
<div class="card card-primary">
    <div class="card-header ml-3 mt-3 mr-3">
    <h3 class="card-title">Update Data Pertanyaan {{ $pertanyaan->id }}</h3>
    </div>
    <form role="form" action="/pertanyaan/{{ $pertanyaan->id }}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
        <label for="judul">Judul Pertanyaan</label>
        <input type="text" class="form-control" id="judul" value="{{ old('judul', $pertanyaan->judul) }}" name="judul" placeholder="Judul Pertanyaan">
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
        <div class="form-group">
        <label for="isi">Isi</label>
        <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $pertanyaan->isi) }}" placeholder="Isi">
        @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
    </div>

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
    </form>
</div>
@endsection