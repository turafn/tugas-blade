@extends('master')

@section('content')

<table class="table mt-3 ml-3 mr-3">
    @if (session('success'))
        <div class="alert alert-success mt-3 ml-3 mr-3">
            {{ session('success') }}
        </div>
    @endif
    <form class="mb-3">
        <a class="btn btn-primary mt-3 ml-3" href="{{ route('pertanyaan.create') }}">Create</a>
    </form>
    <thead class="thead-light">
        <tr>
        <th scope="col">NO</th>
        <th scope="col">Judul</th>
        <th scope="col">Isi</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($pertanyaan as $key =>$pertanyaan)
            <tr>
                <td> {{ $key + 1 }}</td>
                <td> {{ $pertanyaan->judul }} </td>
                <td> {{ $pertanyaan->isi }} </td>
                <td style="display: flex">
                    <a href="{{ route('pertanyaan.show',['pertanyaan' => $pertanyaan->id]) }}" class="btn btn-info btn-sm mr-3">Detail</a>
                    <a href="{{ route('pertanyaan.edit',['pertanyaan' => $pertanyaan->id]) }}" class="btn btn-primary btn-sm mr-3">Edit</a>
                    <form action="{{ route('pertanyaan.destroy',['pertanyaan' => $pertanyaan->id]) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endsection