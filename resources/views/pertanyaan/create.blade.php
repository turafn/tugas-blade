@extends('master')

@section('content')
<div class="card card-primary">
    <div class="card-header ml-3 mt-3 mr-3">
    <h3 class="card-title">Tambah Data Pertanyaan</h3>
    </div>
    <form role="form" action="/pertanyaan" method="POST">
    @csrf
    <div class="card-body">
        <div class="form-group">
        <label for="judul">Judul Pertanyaan</label>
        <input type="text" class="form-control" name="judul" value="{{ old('judul', '') }}" placeholder="Judul Pertanyaan">
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
        <div class="form-group">
        <label for="isi">Isi</label>
        <input type="text" class="form-control" name="isi" value="{{ old('isi', '') }}" placeholder="Isi">
        @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
    </div>

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
    </div>
    </form>
</div>
@endsection