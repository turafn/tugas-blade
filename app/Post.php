<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "pertanyaan";

    /* protected $fillable = ["judul", "isi"]; */
    protected $guarded = [];
}
