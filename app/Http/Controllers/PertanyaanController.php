<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;

class PertanyaanController extends Controller
{
    public function index()
    {
        /* $pertanyaan = DB::table('pertanyaan')->get(); */
        /* dd($post); */
        $pertanyaan  = Post::all();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {

        /* dd($request->all()); */
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        /* $query = DB::table('pertanyaan')->insert([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]); */
        /* $post = new Post;
        $post->judul = $request['judul'];
        $post->isi = $request['isi'];
        $post->save(); */

        $post = Post::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Data berhasil ditambahkan');
    }

    public function show($id)
    {
        /* $pertanyaan = DB::table('pertanyaan')->where('id',$id)->first(); */
        /* dd($pertanyaan); */
        $pertanyaan = Post::find($id);
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        /* $pertanyaan = DB::table('pertanyaan')->where('id',$id)->first(); */
        $pertanyaan = Post::find($id);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        /* $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' =>$request['judul'],
                        'isi' =>$request['isi']
                    ]); */

        $update = Post::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
            ]);
        return redirect('/pertanyaan')->with('success','Data Berhasil di Upadate!!!');
    }

    public function destroy($id)
    {
        /* $query = DB::table('pertanyaan')->where('id',$id)->delete(); */
        Post::destroy($id);
        return redirect('/pertanyaan')->with('success','Data Berhasil diDelete!!!');
    }
}
